#include <stdio.h>
#include <malloc.h>

void swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void bubble_sort(int *arr, int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(&arr[j], &arr[j + 1]);
            }
        }
    }
}

void print_array(int *arr, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
}

int main() {
    int n;
    printf("Enter size of the array: ");
    if (scanf("%d", &n) != 1 || n < 0) {
        printf("Size should be valid positive non-zero integer\n");
        return -1;
    }

    int *array = (int*) malloc(sizeof(int) * n);
    int tmp;
    for (int i = 0; i < n; ++i) {
        printf("Enter array[%d]: ", i);
        if (scanf("%d", &tmp) != 1) {
            printf("Array element should be valid integer\n");
            return -1;
        }
        array[i] = tmp;
    }

    bubble_sort(array, n);
    printf("Sorted array: ");
    print_array(array, n);
    printf("\n");

    free(array);

    return 0;
} 
