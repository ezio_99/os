#include <stdio.h>
#include <malloc.h>

struct LinkedList {
    struct Node *next;
    int size;
};

struct Node {
    int value;
    struct Node *next;
};

struct LinkedList *create_list() {
    struct LinkedList *list = malloc(sizeof(struct LinkedList));
    list->next = NULL;
    list->size = 0;
    return list;
}

void print_list(struct LinkedList *head) {
    if (head->next != NULL) {
        printf("%d", head->next->value);
    }
    struct Node *current = head->next->next;
    while (current != NULL) {
        printf(" %d", current->value);
        current = current->next;
    }
}

int insert_node(struct LinkedList *head, int index, int value) {
    if (index < 0 || index > head->size) {
        return -1;
    }

    struct Node *node = malloc(sizeof(struct Node));
    node->value = value;

    if (head->size == 0) {
        head->next = node;
        node->next = NULL;
    } else if (index == 0) {
        node->next = head->next;
        head->next = node;
    } else {
        struct Node *current = head->next;
        int i = 1;
        while (i != index) {
            current = current->next;
            ++i;
        }
        node->next = current->next;
        current->next = node;
    }
    ++head->size;
    return 0;
}

int delete_node(struct LinkedList *head, int index) {
    if (index < 0 || index >= head->size) {
        return -1;
    }

    if (head->size == 1) {
        free(head->next);
        head->next = NULL;
    } else if (index == 0) {
        struct Node *node_to_delete = head->next;
        head->next = head->next->next;
        free(node_to_delete);
    } else {
        struct Node *current = head->next;
        int i = 0;
        while (i < index - 1) {
            current = current->next;
            ++i;
        }

        struct Node *node_to_delete = current->next;
        current->next = current->next->next;
        free(node_to_delete);
    }
    head->size--;
    return 0;
}

void delete_list(struct LinkedList *head) {
    struct Node *current = head->next;
    struct Node *tmp;
    free(head);
    while (current != NULL) {
        tmp = current;
        current = current->next;
        free(tmp);
    }
}

int main() {
    struct LinkedList *list = create_list();
    insert_node(list, 0, 2);
    insert_node(list, 1, 4);

    print_list(list);
    printf("\n");

    insert_node(list, 1, 3);
    insert_node(list, 0, 1);

    print_list(list);
    printf("\n");

    if (insert_node(list, 10, 3) == -1) {
        printf("IndexError\n");
    }

    if (delete_node(list, 10) == -1) {
        printf("IndexError\n");
    }

    delete_node(list, 1);
    delete_node(list, 1);

    print_list(list);
    printf("\n");

    delete_list(list);

    return 0;
}
