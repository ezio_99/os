#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>

int gcd(int a, int b) {
    return (a == 0) ? b : gcd(b % a, a);
}

int lcm(int a, int b) {
    return (a * b) / gcd(a, b);
}

void *routine(void *args) {
    int *arguments = (int *) args;
    int a = arguments[0];
    int b = arguments[1];
    int result = lcm(a, b);
    printf("lcm(%d, %d) = %d\n", a, b, result);
    free(args);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s <file_name> <amount_of_threads>\n", argv[0]);
        return 1;
    }

    const char *filename = argv[1];
    int amount_of_threads = (int) strtol(argv[2], NULL, 10);

    FILE *file = fopen(filename, "r");

    pthread_t *thread_pool = malloc(amount_of_threads * sizeof(pthread_t));

    int current = 0;
    while (true) {
        int *args = malloc(2 * sizeof(int));

        if (fscanf(file, "%d %d", &args[0], &args[1]) == EOF) {
            free(args);
            break;
        }

        if (current >= amount_of_threads) {
            for (int i = 0; i < amount_of_threads; i++) {
                pthread_join(thread_pool[i], NULL);
            }
            current = 0;
        }

        pthread_create(&thread_pool[current], NULL, routine, args);
        current++;
    }

    for (int i = 0; i < amount_of_threads; i++) {
        pthread_join(thread_pool[i], NULL);
    }

    free(thread_pool);

    return 0;
}
