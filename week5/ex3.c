#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>

struct {
    int count;
    int capacity;
    int *products;
} state;

void *producer(void *vararg) {
    while (true) {
        // if race condition happens, stop everything
        if (state.count > state.capacity) {
            printf("FATAL ERROR! Trying to produce to a full buffer\n");
            exit(1);
        }

        // products array is full - wait
        while (state.count == state.capacity);

        // insert product
        state.products[state.count] = state.count;
        state.count++;
        printf("Produced\n");
    }
}

void *consumer(void *vararg) {
    while (true) {
        // if race condition happens, stop everything
        if (state.count < 0) {
            printf("FATAL ERROR! Trying to consume from an empty buffer\n");
            exit(1);
        }

        // products array is empty - wait
        while (state.count == 0);

        // consume product
        state.products[state.count] = 0;
        state.count--;
        printf("Consumed\n");
    }
}

int main() {
    state.capacity = 5;
    state.count = 0;
    state.products = (int *) malloc(state.capacity * sizeof(int));

    pthread_t prod, cons;

    pthread_create(&prod, NULL, producer, NULL);
    pthread_create(&cons, NULL, consumer, NULL);

    pthread_join(prod, NULL);
    pthread_join(cons, NULL);

    return 0;
}
