#include <stdio.h>
#include <pthread.h>

#define N 3

void *show(void *i) {
    printf("Hello from thread with index = %d\n", *(int *) i);
    printf("Exiting from thread with index = %d\n", *(int *) i);
    pthread_exit(0);
}

int main() {
    for (int i = 1; i <= N; i++) {
        pthread_t tid;
        pthread_create(&tid, NULL, show, &i);
        printf("Creating thread with index = %d\n", i);
    }
    return 0;
}
