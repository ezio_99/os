#!/usr/bin/env bash

N=5

if [ $# -eq 1 ]; then
  N=$1
elif [ $# -gt 1 ]; then
  echo "Usage: ${0} [execution count]"
  exit
fi

echo 1 >ex2.1.txt

# Critical region is file with numbers

for ((i = 1; i <= N; i++)); do
  # race condition usually appears after 4'th iteration
  ./add_number.sh ex2.1.txt &
  ./add_number.sh ex2.1.txt
done
