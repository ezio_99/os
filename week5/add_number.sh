#!/usr/bin/env bash

if [ $# -eq 1 ]; then
  file_name="$1"
else
  echo "Usage: ${0} <file_name>"
  exit
fi

last_number=$(tail -n 1 <"$file_name")
last_number=$((last_number + 1))

echo $last_number >>"$file_name"
