#!/bin/bash

N=5

if [ $# -eq 1 ]; then
  N=$1
elif [ $# -gt 1 ]; then
  echo "Usage: ${0} [execution count]"
  exit
fi

echo 1 >ex2.2.txt

# Critical region is file with numbers

for ((i = 1; i <= N; i++)); do
  ./add_number_lock.sh ex2.2.txt &
  ./add_number_lock.sh ex2.2.txt
done
