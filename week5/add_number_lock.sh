#!/usr/bin/env bash

if [ $# -eq 1 ]; then
  file_name="$1"
else
  echo "Usage: ${0} <file_name>"
  exit
fi

# if lock file exists, we wait
while ! ln "$file_name" "$file_name.lock" 2>/dev/null; do
  sleep 0.1
done

last_number=$(tail -n 1 <"$file_name")
last_number=$((last_number + 1))

echo $last_number >>"$file_name"

# delete the lock file
rm "$file_name.lock"