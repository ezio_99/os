#!/usr/bin/env bash

buffer_sizes=(1028 2048 4096)
file_sizes=(1024 5120 10240)
files=(1MB 5MB 10MB)

echo "Compiling copy.c with buffer sizes ${buffer_sizes[*]} bytes"
for size in ${buffer_sizes[*]}; do
  gcc ./copy.c -DBUF_SIZE="$size" -o "copy${size}"
  gcc ./copy.c -DBUF_SIZE="$size" -DSYNC -o "copy${size}_sync"
done

echo "Creating sample data"
for i in 0 1 2; do
  dd bs=1024 count="${file_sizes[${i}]}" if=/dev/urandom of="${files[${i}]}"
done
echo "Created files: ${files[*]}"

echo "Checking copy.c with various parameters"

echo "Results:" | tee ex3.txt
for size in ${buffer_sizes[*]}; do
  echo "buffer size: $size bytes" | tee -a ex3.txt
  for file in ${files[*]}; do
    echo -e -n "\tfile size: ${file}" | tee -a ex3.txt
    { time ./copy"$size" "$file" temp.txt; } 2>&1 1>/dev/null | sed 's/^/\t\t/' | tee -a ex3.txt
    echo -e -n "\tfile size: $file (O_SYNC)" | tee -a ex3.txt
    { time ./copy"$size"_sync "$file" temp.txt; } 2>&1 1>/dev/null | sed 's/^/\t\t/' | tee -a ex3.txt
  done
done

echo "Testing done"

for size in ${buffer_sizes[*]}; do
  rm "copy${size}" "copy${size}_sync"
done

for file in ${files[*]}; do
  rm "$file"
done

rm temp.txt

echo "All files used for testing deleted"
