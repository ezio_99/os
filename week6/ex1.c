#include <stdio.h>
#include <stdlib.h>
#include "helpers.c"

/*
 * FCFS -- simple implementation,
 * but not good characteristics
 */

int main() {
    int n = get_processes_number();
    int *AT, *BT;
    get_processes(&AT, &BT, n);

    // sorting processes by Arrival time
    sort(AT, BT, 0, n - 1);

    int *CT = (int *) malloc(sizeof(int) * n);
    int *TAT = (int *) malloc(sizeof(int) * n);
    int *WT = (int *) malloc(sizeof(int) * n);
    CT[0] = AT[0] + BT[0];
    WT[0] = 0;
    for (int i = 1; i < n; i++) {
        if (AT[i] <= CT[i - 1]) {
            CT[i] = BT[i] + CT[i - 1];
        } else {
            CT[i] = AT[i] + BT[i];
        }
        WT[i] = CT[i - 1] - AT[i];
        if (WT[i] < 0) {
            WT[i] = 0;
        }
    }
    for (int i = 0; i < n; i++) {
        TAT[i] = BT[i] + WT[i];
    }

    float AVG_TAT, AVG_WT;
    get_averages(TAT, WT, &AVG_TAT, &AVG_WT, n);

    printf("\n");
    print_results(AT, BT, CT, TAT, WT, n);
    printf("\n");
    print_averages(AVG_TAT, AVG_WT);

    free_arrays(AT, BT, CT, TAT, WT);

    return 0;
}
