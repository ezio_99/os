#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "helpers.c"

/*
 * Round Robin -- good for interactive systems
 * better than FSFS, worse than SJN
 */


int main() {
    int *AT, *BT;
    int quantum = get_quantum();
    int n = get_processes_number();
    get_processes(&AT, &BT, n);

    // sorting processes by Arrival time
    sort(AT, BT, 0, n - 1);

    int *CT = (int *) malloc(sizeof(int) * n);
    int *TAT = (int *) malloc(sizeof(int) * n);
    int *WT = (int *) malloc(sizeof(int) * n);

    int *CBT = (int *) malloc(sizeof(int) * n); // current Burst time of processes
    for (int i = 0; i < n; ++i) {
        CBT[i] = BT[i];
    }

    int time = 0;
    int working_processes_count = n;
    int no_processed;
    while (working_processes_count > 0) {
        no_processed = true;
        for (int i = 0; i < n; ++i) {
            if (CBT[i] <= 0) continue;  // process already done
            if (AT[i] > time) continue;  // process' arrival time in future
            if (CBT[i] <= quantum) {
                CT[i] = time + CBT[i];
                time = CT[i];
                CBT[i] = 0;  // process processed
                --working_processes_count;
                no_processed = false;
            } else {  // CBT[i] > quantum
                CBT[i] -= quantum;
                time += quantum;
                no_processed = false;
            }
        }
        if (no_processed) { // there are no process in this time
            int min = AT[n - 1];
            for (int i = 0; i < n; ++i) {
                if (CBT[i] > 0 && min > AT[i]) {
                    min = AT[i];
                }
            }
            time = min;
        }
    }

    for (int i = 0; i < n; i++) {
        WT[i] = CT[i] - AT[i] - BT[i];
        if (WT[i] < 0) {
            WT[i] = 0;
        }
        TAT[i] = BT[i] + WT[i];
    }

    float AVG_TAT, AVG_WT;
    get_averages(TAT, WT, &AVG_TAT, &AVG_WT, n);

    printf("\n");
    print_results(AT, BT, CT, TAT, WT, n);
    printf("\n");
    print_averages(AVG_TAT, AVG_WT);

    free_arrays(AT, BT, CT, TAT, WT);
    free(CBT);

    return 0;
}
