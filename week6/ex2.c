#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "helpers.c"

/*
 * SJN -- great performance, especially in batch systems
 * have better characteristics than SJN or FCFS
 */

int main() {
    int *AT, *BT;
    int n = get_processes_number();
    get_processes(&AT, &BT, n);

    // sorting processes by Arrival time
    sort(AT, BT, 0, n - 1);

    int *CT = (int *) malloc(sizeof(int) * n);
    int *TAT = (int *) malloc(sizeof(int) * n);
    int *WT = (int *) malloc(sizeof(int) * n);

    int time = 0;
    bool all_processes_sorted = false;
    bool need_to_sort_all_processes;
    for (int index = 0; index < n; ++index) {
        if (AT[index] > time) {
            time = AT[index];
        }
        if (!all_processes_sorted) {
            need_to_sort_all_processes = true;
            for (int i = index; i < n; ++i) {
                if (time < AT[i]) {
                    sort(BT, AT, index, i - 1);
                    need_to_sort_all_processes = false;
                    break;
                }
            }
            if (need_to_sort_all_processes) {
                sort(BT, AT, index, n - 1);
                all_processes_sorted = true;
            }
        }
        CT[index] = time + BT[index];
        time = CT[index];
    }

    WT[0] = 0;
    TAT[0] = BT[0] + WT[0];
    for (int i = 1; i < n; i++) {
        WT[i] = CT[i - 1] - AT[i];
        if (WT[i] < 0) {
            WT[i] = 0;
        }
        TAT[i] = BT[i] + WT[i];
    }

    float AVG_TAT, AVG_WT;
    get_averages(TAT, WT, &AVG_TAT, &AVG_WT, n);

    printf("\n");
    print_results(AT, BT, CT, TAT, WT, n);
    printf("\n");
    print_averages(AVG_TAT, AVG_WT);

    free_arrays(AT, BT, CT, TAT, WT);

    return 0;
}
