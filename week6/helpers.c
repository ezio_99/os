void swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void sort(int *arr1, int *arr2, int low, int high) {
    for (int i = low; i <= high; i++) {
        for (int j = low; j < high - i; j++) {
            if (arr1[j] > arr1[j + 1]) {
                swap(&arr1[j], &arr1[j + 1]);
                swap(&arr2[j], &arr2[j + 1]);
            }
        }
    }
}

int get_processes_number() {
    printf("Enter number of processes: ");
    int n;
    if (scanf("%d", &n) != 1 || n < 1) {
        printf("Number of processes should be valid positive non-zero integer\n");
        exit(1);
    }
    return n;
}

int get_quantum() {
    printf("Enter quantum: ");
    int quantum;
    if (scanf("%d", &quantum) != 1 || quantum < 1) {
        printf("Quantum should be valid positive non-zero integer\n");
        exit(1);
    }
    return quantum;
}

void get_processes(int **at, int **bt, int n) {
    int *AT = (int *) malloc(sizeof(int) * n);
    int *BT = (int *) malloc(sizeof(int) * n);
    int tmp;
    for (int i = 1; i <= n; ++i) {
        printf("Enter Arrival time for process[%d]: ", i);
        if (scanf("%d", &tmp) != 1 || n < 0) {
            printf("Arrival time should be valid positive integer\n");
            exit(1);
        }
        AT[i - 1] = tmp;
        printf("Enter Burst   time for process[%d]: ", i);
        if (scanf("%d", &tmp) != 1 || n < 1) {
            printf("Burst time should be non-zero valid positive integer\n");
            exit(1);
        }
        BT[i - 1] = tmp;
    }

    *at = AT;
    *bt = BT;
}

void print_array(int *a, int n) {
    for (int i = 0; i < n - 1; ++i) {
        printf("%d ", a[i]);
    }
    printf("%d\n", a[n - 1]);
}

void free_arrays(int *a, int *b, int *c, int *d, int *e) {
    free(a);
    free(b);
    free(c);
    free(d);
    free(e);
}

void get_averages(const int *arr1, const int *arr2, float *avg1, float *avg2, int n) {
    int sum1 = 0, sum2 = 0;
    for (int i = 0; i < n; i++) {
        sum1 += arr1[i];
        sum2 += arr2[i];
    }
    *avg1 = (float) sum1 / (float) n;
    *avg2 = (float) sum2 / (float) n;
}

void print_results(int *AT, int *BT, int *CT, int *TAT, int *WT, int n) {
    printf("P#\t AT\t BT\t CT\t TAT\t WT\n");
    for (int i = 0; i < n; i++) {
        printf("P%d\t %d\t %d\t %d\t %d\t %d\n", i + 1, AT[i], BT[i], CT[i], TAT[i], WT[i]);
    }
}

void print_averages(float AVG_TAT, float AVG_WT) {
    printf("Average Turnaround Time = %f\n", AVG_TAT);
    printf("Average WT = %f\n", AVG_WT);
}
