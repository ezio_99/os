#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>

int main() {
    int file = open("./ex1.txt", O_RDWR);
    const char *text = "This is a nice day";
    size_t len = strlen(text);
    ftruncate(file, len);
    char *m = mmap(0, len, PROT_READ | PROT_WRITE, MAP_SHARED, file, 0);
    memcpy(m, text, len);
    munmap(m, len);
    close(file);

    return 0;
}
