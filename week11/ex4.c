#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>

#define input "./ex1.txt"
#define output "./ex1.memcpy.txt"

int main() {
    char *src, *dest;
    size_t filesize;

    int input_file = open(input, O_RDONLY);
    filesize = lseek(input_file, 0, SEEK_END);
    src = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, input_file, 0);

    int output_file = open(output, O_RDWR | O_CREAT, 0666);
    ftruncate(output_file, filesize);
    dest = mmap(NULL, filesize, PROT_READ | PROT_WRITE, MAP_SHARED, output_file, 0);

    memcpy(dest, src, filesize);

    munmap(src, filesize);
    munmap(dest, filesize);

    close(input_file);
    close(output_file);

    return 0;
}
