#include <stdio.h>
#include <zconf.h>
#include <memory.h>

int main() {
    setvbuf(stdout, NULL, _IOLBF, BUFSIZ);
    const char * hello = "Hello";
    for (int i = 0; i < strlen(hello); ++i) {
        printf("%c", hello[i]);
        sleep(1);
    }

    return 0;
}
