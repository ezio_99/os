#include <stdio.h>
#include <unistd.h>

int main() {
    int n = 10;
    pid_t p = fork();
    if (p == 0) {
        printf("Hello from child [%d - %d]\n", getpid(), n);
    } else if (p > 0) {
        printf("Hello from parent [%d - %d]\n", getpid(), n);
    } else {
        return 1;
    }
    return 0;
}
