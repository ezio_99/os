#!/usr/bin/env bash

gcc ex1.c -o ex1

for _ in {0..9}; do
  ./ex1
done

# The strings from the parent and child process will be shuffled,
# because they ran simultaneously on several cores of CPU
# In this particular example they are not shuffled,
# because processes end before starting new
# If we increase the number of runs, we will see shuffling

#Hello from parent [80549 - 10]
#Hello from child [80550 - 10]
#Hello from parent [80551 - 10]
#Hello from child [80552 - 10]
#Hello from parent [80553 - 10]
#Hello from child [80554 - 10]
#Hello from parent [80555 - 10]
#Hello from child [80556 - 10]
#Hello from parent [80557 - 10]
#Hello from child [80558 - 10]
#Hello from parent [80559 - 10]
#Hello from child [80560 - 10]
#Hello from parent [80561 - 10]
#Hello from child [80562 - 10]
#Hello from parent [80563 - 10]
#Hello from child [80564 - 10]
#Hello from parent [80565 - 10]
#Hello from child [80566 - 10]
#Hello from parent [80567 - 10]
#Hello from child [80568 - 10]
