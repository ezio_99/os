#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

char* double_size(char* string, int max_size) {
    char* new_string = malloc(sizeof(char) * max_size * 2);
    strcpy(new_string, string);
    free(string);
    return new_string;
}

char *get_command() {
    int size = 16;
    char* command = malloc(sizeof(char) * size);
    int index = -1;
    char c;
    while (c = getchar()) {
        index += 1;

        if (c == '\n') {
            break;
        }

        if (index == size - 1) {
            command[index] = '\0';
            command = double_size(command, size);
            size *= 2;
        }

        command[index] = c;
    }

    if (index == 0) {
        command[0] = ' ';
        command[1] = '\0';
    } else {
        command[index] = '\0';
    }

    return command;
}

int main() {
    printf("Please enter your commands (for exit use exit command):\n");
    char *command = NULL;
    while (true) {
        command = get_command();
        if (strcmp(command, "exit") == 0) {
            free(command);
            break;
        } else if (strcmp(command, "") == 0) {
            continue;
        }
        system(command);
        free(command);
    }
    return 0;
}
