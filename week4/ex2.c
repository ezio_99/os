#include <stdio.h>
#include <unistd.h>

#define N 3

int main() {
    for (int i = 0; i < N; ++i) {
        fork();
        printf("Hello from %d\n", getpid());
        sleep(5);
    }

    return 0;
}

/*
 * After every fork, we double number of processes
 * 1 -> 2 -> 4 -> 8 for 3 forks
 * 1 -> 2 -> 4 -> 8 -> 16 -> 32 for 5 forks
 * We can conclude, that number of processes equal to 2^number_of_forks
 */

/* N = 3

ex2─┬─ex2─┬─ex2───ex2
    │     └─ex2
    ├─ex2───ex2
    └─ex2

Hello from 453506
Hello from 453507
Hello from 453506
Hello from 453520
Hello from 453507
Hello from 453521
Hello from 453506
Hello from 453520
Hello from 453549
Hello from 453550
Hello from 453507
Hello from 453551
Hello from 453521
Hello from 453552

*/

/* N = 5

ex2─┬─ex2─┬─ex2─┬─ex2─┬─ex2───ex2
    │     │     │     └─ex2
    │     │     ├─ex2───ex2
    │     │     └─ex2
    │     ├─ex2─┬─ex2───ex2
    │     │     └─ex2
    │     ├─ex2───ex2
    │     └─ex2
    ├─ex2─┬─ex2─┬─ex2───ex2
    │     │     └─ex2
    │     ├─ex2───ex2
    │     └─ex2
    ├─ex2─┬─ex2───ex2
    │     └─ex2
    ├─ex2───ex2
    └─ex2

Hello from 453141
Hello from 453142
Hello from 453141
Hello from 453156
Hello from 453142
Hello from 453157
Hello from 453141
Hello from 453156
Hello from 453173
Hello from 453174
Hello from 453142
Hello from 453157
Hello from 453176
Hello from 453175
Hello from 453141
Hello from 453190
Hello from 453156
Hello from 453173
Hello from 453192
Hello from 453191
Hello from 453174
Hello from 453142
Hello from 453157
Hello from 453195
Hello from 453193
Hello from 453194
Hello from 453176
Hello from 453196
Hello from 453175
Hello from 453197
Hello from 453190
Hello from 453216
Hello from 453141
Hello from 453173
Hello from 453156
Hello from 453192
Hello from 453174
Hello from 453157
Hello from 453218
Hello from 453176
Hello from 453197
Hello from 453193
Hello from 453175
Hello from 453221
Hello from 453194
Hello from 453219
Hello from 453142
Hello from 453217
Hello from 453226
Hello from 453191
Hello from 453227
Hello from 453228
Hello from 453195
Hello from 453196
Hello from 453225
Hello from 453220
Hello from 453223
Hello from 453224
Hello from 453229
Hello from 453230
Hello from 453222
Hello from 453231

*/
