#include <stdio.h>

void draw_symbol(int count, char c) {
    for (int i = 0; i < count; ++i) {
        putchar(c);
    }
}

void draw_triangle(int n) {
    int asterisks_count = 1;
    int blanks_count = n;
    int max_symbols = 2 * n - 1;
    while (asterisks_count != max_symbols) {
        blanks_count -= 1;
        draw_symbol(blanks_count, ' ');
        draw_symbol(asterisks_count, '*');
        draw_symbol(blanks_count, ' ');
        putchar('\n');
        asterisks_count += 2;
    }
    draw_symbol(max_symbols, '*');
}

void draw_half_rectangle(int n) {
    for (int i = 1; i < n; ++i) {
        draw_symbol(i, '*');
        putchar('\n');
    }
    draw_symbol(n, '*');
}

void draw_arrow(int n) {
    for (int i = 1; i < n; ++i) {
        draw_symbol(i, '*');
        putchar('\n');
    }
    for (int i = n; i > 1; --i) {
        draw_symbol(i, '*');
        putchar('\n');
    }
    draw_symbol(1, '*');
}

void draw_rectangle(int n) {
    for (int i = 0; i < 2 * n; ++i) {
        draw_symbol(n, '*');
        putchar('\n');
    }
    draw_symbol(n, '*');
}

int main(int argc, char *argv[]) {
    int n;

    if (argc != 2) {
        printf("Should be 1 command line parameter\n");
        return -1;
    }

    if (sscanf(argv[1], "%i", &n) != 1 || n <= 0) {
        printf("Bad command line parameter (should be valid positive non-zero integer)\n");
        return -1;
    }

    printf("Choose which figure to draw (enter number of option):\n");
    printf("1 - Triangle\n");
    printf("2 - Half rectangle\n");
    printf("3 - Arrow\n");
    printf("4 - Rectangle\n");
    printf("0 - Exit\n");
    printf("Your choice: ");

    char choice[2];
    fgets(choice, 2, stdin);
    int menu_option;
    int status = sscanf(choice, "%i", &menu_option);
    if (status != 1) {
        printf("\nInvalid choice");
        return -1;
    }
    switch (menu_option) {
        case 0:
            return 0;
        case 1:
            draw_triangle(n);
            break;
        case 2:
            draw_half_rectangle(n);
            break;
        case 3:
            draw_arrow(n);
            break;
        case 4:
            draw_rectangle(n);
            break;
    }

    putchar('\n');

    return 0;
}
