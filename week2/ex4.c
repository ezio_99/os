#include <stdio.h>

void swap_ints(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

#define INT_STR_LEN 11
int read_int(char msg[]) {
    char tmp_str[INT_STR_LEN];
    int i;
    printf("%s", msg);
    fgets(tmp_str, INT_STR_LEN, stdin);
    sscanf(tmp_str, "%i", &i);
    return i;
}

int main(int argc, char *argv[]) {
    int a = read_int("Enter first integer: ");
    int b = read_int("Enter second integer: ");
    swap_ints(&a, &b);
    printf("Now first integer = %i, second integer = %i\n", a, b);

    return 0;
}
