#include <stdio.h>
#include <string.h>

#define STRING_LENGTH 256

int main() {
    char str[STRING_LENGTH];
    printf("Enter your string: ");
    fgets(str, STRING_LENGTH, stdin);
    printf("Reversed string: ");
    for (int i = (int) strlen(str) - 1; i > -1; --i) {
        if (str[i] != '\n') {
            putchar(str[i]);
        }
    }

    putchar('\n');

    return 0;
}
