#include <stdio.h>
#include <float.h>
#include <limits.h>

int main() {
    int i;
    float f;
    double d;

    i = INT_MAX;
    f = FLT_MAX;
    d = DBL_MAX;

    printf("integer: value = %i, size = %lu\n", i, sizeof(i));
    printf("  float: value = %f, size = %lu\n", f, sizeof(f));
    printf(" double: value = %f, size = %lu\n", d, sizeof(d));

    return 0;
}
