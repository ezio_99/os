#include <stdio.h>

void draw_symbol(int count, char c) {
    for (int i = 0; i < count; ++i) {
        putchar(c);
    }
}

void draw_triangle(int n) {
    int asterisks_count = 1;
    int blanks_count = n;
    int max_symbols = 2 * n - 1;
    while (asterisks_count != max_symbols) {
        blanks_count -= 1;
        draw_symbol(blanks_count, ' ');
        draw_symbol(asterisks_count, '*');
        draw_symbol(blanks_count, ' ');
        putchar('\n');
        asterisks_count += 2;
    }
    draw_symbol(max_symbols, '*');
}

int main(int argc, char *argv[]) {
    int n;

    if (argc != 2) {
        printf("Should be 1 command line parameter\n");
        return -1;
    }

    if (sscanf(argv[1], "%i", &n) != 1 || n < 0) {
        printf("Bad command line parameter (should be valid positive non-zero integer)\n");
        return -1;
    }

    draw_triangle(n);
    putchar('\n');

    return 0;
}
