#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_PROCESSES 20
#define MAX_RESOURCES 20
#define MAX_LINE 40

void check_file_is_open(FILE *file) {
    if (file == NULL) {
        printf("%s\n", "Error: cannot open file\n");
        exit(2);
    }
}

void raise_malformed_error() {
    printf("Error: Malformed input file\n");
    exit(1);
}

bool check_is_empty_line(FILE *file) {
    return fgetc(file) == '\n';
}

void check_deadlocks(int prosesses_quantity, int available_resourses_quantity, const char *output_filename, int *A,
                     int C[MAX_PROCESSES][MAX_RESOURCES], int R[MAX_PROCESSES][MAX_RESOURCES]) {
    bool process_is_finished[MAX_PROCESSES] = {};

    int j;
    while (true) {
        bool can_be_completed = false;
        for (int i = 0; i < prosesses_quantity; i++) {
            if (process_is_finished[i]) continue;

            for (j = 0; j < available_resourses_quantity; j++) {
                if (R[i][j] > A[j]) break;
            }

            if (j != available_resourses_quantity) continue;

            for (j = 0; j < available_resourses_quantity; j++) {
                A[j] += R[i][j] + C[i][j];
            }
            can_be_completed = true;
            process_is_finished[i] = true;
        }
        if (!can_be_completed) break;
    }

    FILE *output = fopen(output_filename, "w");

    check_file_is_open(output);

    bool deadlock = false;
    for (int i = 0; i < prosesses_quantity; i++) {
        if (!process_is_finished[i]) {
            deadlock = true;
            break;
        }
    }

    if (deadlock) {
        fprintf(output, "Deadlock between processes: ");
        for (int i = 0; i < prosesses_quantity; i++) {
            if (!process_is_finished[i]) {
                fprintf(output, "%d, ", i);
            }
        }
        fprintf(output, "\n");
    } else {
        fprintf(output, "No deadlock\n");
    }
}

int main(int argc, char *argv[]) {
    const char *input_filename = argc >= 2 ? argv[1] : "input.txt";
    const char *output_filename = argc >= 3 ? argv[2] : "output.txt";

    FILE *input = fopen(input_filename, "r");
    check_file_is_open(input);

    char buffer[MAX_LINE];

    int E[MAX_RESOURCES], A[MAX_RESOURCES];
    int C[MAX_PROCESSES][MAX_RESOURCES], R[MAX_PROCESSES][MAX_RESOURCES];

    fgets(buffer, MAX_LINE, input);
    char *p = buffer;
    int existing_resourses_quantity = 0;
    while (*p != '\n') {
        int temp = (int) strtol(p, &p, 10);
        E[existing_resourses_quantity++] = temp;
    }

    // skip 1 line
    if (!check_is_empty_line(input)) raise_malformed_error();

    fgets(buffer, MAX_LINE, input);
    p = buffer;
    int available_resourses_quantity = 0;
    while (*p != '\n') {
        int temp = (int) strtol(p, &p, 10);
        A[available_resourses_quantity++] = temp;
    }

    // skip 1 line
    if (!check_is_empty_line(input)) raise_malformed_error();

    // check if length of E == length of A
    if (existing_resourses_quantity != available_resourses_quantity) raise_malformed_error();

    int tmp;
    int processes_quantity = 0;
    while (true) {
        fgets(buffer, MAX_LINE, input);
        if (strcmp(buffer, "\n") == 0) break;
        p = buffer;
        for (int i = 0; i < existing_resourses_quantity; i++) {
            tmp = (int) strtol(p, &p, 10);
            C[processes_quantity][i] = tmp;
        }
        processes_quantity++;
    }

    for (int i = 0; i < processes_quantity; i++) {
        fgets(buffer, MAX_LINE, input);
        p = buffer;
        for (int j = 0; j < existing_resourses_quantity; j++) {
            tmp = (int) strtol(p, &p, 10);
            R[i][j] = tmp;
        }
    }

    check_deadlocks(processes_quantity, available_resourses_quantity, output_filename, A, C, R);

    return 0;
}
