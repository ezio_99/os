#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/resource.h>

int main() {
    const int MB = 1024 * 1024;
    int *allocated_memory = NULL;
    size_t size = 0;
    struct rusage usage_info;
    const char *line_divider = "----------------------------------------------------------------------------------------";
    printf("%s\nUser CPU time | System CPU time | Soft page faults | Hard page faults | Context switches\n%s\n",
           line_divider, line_divider);
    for (int i = 0; i < 10; ++i) {
        size += 10 * MB;
        allocated_memory = realloc(allocated_memory, size);
        memset(allocated_memory, 0, size);
        getrusage(RUSAGE_SELF, &usage_info);
        printf("%-13ld | %-15ld | %-16ld | %-16ld | %-16ld\n%s\n", usage_info.ru_utime.tv_usec,
               usage_info.ru_stime.tv_usec, usage_info.ru_minflt, usage_info.ru_majflt,
               usage_info.ru_nvcsw + usage_info.ru_nivcsw, line_divider);
        sleep(1);
    }
    free(allocated_memory);
    return 0;
}
