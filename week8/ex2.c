#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/*
 * When we call realloc(), program's memory consumption increase
 * we can see this in decreasing values in
 * column "free" in vmstat's output.
 *
 * In si, so columns I have zeros because
 * my memory is large and swap is not used at this moment of time.
 */

int main() {
    const int MB = 1024 * 1024;
    int *allocated_memory = NULL;
    size_t size = 0;
    for (int i = 0; i < 10; ++i) {
        size += 10 * MB;
        allocated_memory = realloc(allocated_memory, size);
        memset(allocated_memory, 0, size);
        sleep(1);
    }
    free(allocated_memory);
    return 0;
}
