#include <dirent.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <stdbool.h>

#define dir_to_open "./tmp"
#define max_files_quantity 10
#define max_name_length 100

struct file {
    unsigned long inode;
    char names[max_files_quantity][max_name_length];
    int size;
};

void init_files_array(struct file *array) {
    for (int i = 0; i < max_files_quantity; ++i) {
        array[i].inode = 0;
        array[i].size = 0;
    }
}

int main() {
    DIR *dir = opendir(dir_to_open);
    if (dir == NULL) {
        printf("Error: cannot open directory %s\n", dir_to_open);
        exit(1);
    }

    struct dirent *current_entry;

    struct file files[max_files_quantity];
    init_files_array(files);
    int files_quantity = 0;
    bool is_found;
    struct stat stat_info = {};

    while ((current_entry = readdir(dir)) != NULL) {
        if(current_entry->d_name[0] == '.') continue;

        char dir_name[4+max_name_length] = "tmp/";
        if (stat(strcat(dir_name, current_entry->d_name), &stat_info) != 0) {
            printf("Error: cannot read stat for file %s\n", current_entry->d_name);
            break;
        }

        if (stat_info.st_nlink < 2) continue;

        is_found = false;
        for (int i = 0; i < max_files_quantity; ++i) {
            if (files[i].inode == stat_info.st_ino) {
                strncpy(files[i].names[files[i].size], current_entry->d_name, max_name_length);
                files[i].inode = stat_info.st_ino;
                files[i].size++;
                is_found = true;
                break;
            }
        }
        if (!is_found) {
            files[files_quantity].inode = stat_info.st_ino;
            strncpy(files[files_quantity].names[0], current_entry->d_name, max_name_length);
            files[files_quantity].size++;
            files_quantity++;
        }
    }

    closedir(dir);

    if (files_quantity == 0) {
        printf("There are no files with more than more than 1 hard link\n");
        exit(0);
    }

    printf("There are %d files with more than 1 hard link:\n", files_quantity);
    for (int i = 0; i < files_quantity; ++i) {
        printf("inode = %lu; names = [%s", files[i].inode, files[i].names[0]);
        for (int j = 1; j < files[i].size; ++j) {
            printf(", %s", files[i].names[j]);
        }
        printf("]; total %d files\n", files[i].size);
    }

    return 0;
}
