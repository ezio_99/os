#!/usr/bin/env bash

touch ../week1/file.txt
link ../week1/file.txt _ex2.txt
inum=$(ls -i _ex2.txt | cut -d ' ' -f 1)
find ../ -inum "$inum"
find ../ -inum "$inum" -exec rm {} \;
