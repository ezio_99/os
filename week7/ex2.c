#include <stdio.h>
#include <stdlib.h>

int get_number_of_elements() {
    printf("Enter number of elements in array: ");
    int n;
    if (scanf("%d", &n) != 1 || n < 1) {
        printf("Number of elements should be valid positive non-zero integer\n");
        exit(1);
    }
    return n;
}

int main() {
    int N = get_number_of_elements();
    int *array = (int *) malloc(sizeof(int) * N);
    for (int i = 0; i < N; ++i) {
        array[i] = i;
    }
    for (int i = 0; i < N - 1; ++i) {
        printf("%d ", array[i]);
    }
    printf("%d ", array[N - 1]);
    free(array);

    return 0;
}
