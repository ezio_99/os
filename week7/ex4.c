#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define min(x, y) (((x) < (y)) ? (x) : (y))

// old_size added for knowing how much to copy
void *my_realloc(void *ptr, size_t new_size, size_t old_size) {
    if (new_size == 0) {
        free(ptr);
        return NULL;
    }

    if (ptr == NULL) {
        return malloc(new_size);
    }

    void *new_ptr = malloc(new_size);
    if (new_ptr == NULL) {
        return NULL;
    }

    size_t min_size = min(old_size, new_size);
    memcpy(new_ptr, ptr, min_size);

    free(ptr);

    return new_ptr;
}

void print_array(int *array, int size) {
    if (array == NULL) return;
    for (int i = 0; i < size - 1; ++i) {
        printf("%d, ", array[i]);
    }
    printf("%d\n", array[size - 1]);
}

int main() {
    int size = 5, old_size;
    int *array = (int *) my_realloc(NULL, size * sizeof(int), 0);
    for (int i = 0; i < size; i++) {
        array[i] = i;
    }
    printf("Array: ");
    print_array(array, size);

    old_size = size;
    size = 10;
    array = (int *) my_realloc(array, size * sizeof(int), old_size * sizeof(int));
    printf("Array after increasing: ");
    print_array(array, size);

    old_size = size;
    size = 2;
    array = (int *) my_realloc(array, size * sizeof(int), old_size * sizeof(int));
    printf("Array after decreasing: ");
    print_array(array, size);

    array = (int *) my_realloc(array, 0, 0);
    printf("Array pointer after freeing: %p\n", array);

    return 0;
}